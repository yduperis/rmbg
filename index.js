#!/usr/bin/env node
import {removeBackground} from "@imgly/background-removal-node";
import * as path from "path";
import * as fs from "fs";

if (process.argv.length !== 3) {
    console.error("Expect the image to be passed as single argument")
    process.exit(1);
}

const imgSrc = process.argv[2];
const imgDst = `${path.resolve(imgSrc, "..")}/${path.basename(imgSrc).split(".")[0]}.nobg.png`

removeBackground(imgSrc).then(async blob => {
    fs.writeFileSync(imgDst, Buffer.from(await blob.arrayBuffer()));
})